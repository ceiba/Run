<?php
/**
 +------------------------------------------------------------------------------
 * Run Framework RedisCRUD操作类
 +------------------------------------------------------------------------------
 * @date    17-07
 * @author Jimmy Wang <1105235512@qq.com>
 * @version 1.0
 +------------------------------------------------------------------------------
 */
class RedisCrud extends RunRedis{
    
    public function __construct($key=''){
	$this->key = $key;
    }
    
    /**
     * 查询所有数据
     */
    public function findAll(){
	    if($this->get($this->key)){
		for( $i = 1; $i <= $this->get($this->key); $i++ ){
		    $data[] = $this->hGetAll($this->key.':'.$i);
		    $data = array_filter($data);//过滤数组中的空元素
		}
		 return $data;
	    }
	   return false;
    }
    
	/**
	 * 添加数据
	 * @param array $data array
	 */
	public function add($data){
	     $id = $this->incr($this->key);
	     $data['id'] = $id;
	     $bool = $this->hMset($this->key.':'.$id, $data);
             return $bool;
	}
	
	
	public function findOne($id){
	    return $this->hGetAll($this->key.':'.$id);
	}
	
	public function edit($id,$data){
	     $bool = $this->hMset($this->key.':'.$id, $data);
             return $bool;
	}
	
	public function delete($id){
	    $bool = $this->del($this->key.':'.$id);
            return $bool;
	}
}

