<?php
class DbHook
{
	/**
	 * SQL语句
	 */
	public  $sql    = null;

	/**
	 * SQL日志
	 */
	public  $sqlLog = null;

	/**
	 * 开始执行SQL的时间
	 */
	private $startTime  = null;

	/**
	 * 结束时间
	 */
	private $endTime    = null;

	/**
	 * SQL计数器
	 */
	private $loop  = 0;

	/**
	 * 记录SQL日志
	 *
	 * @access public
	 * @return void
	 */
	public function work()
	{
		if(defined('IS_MYSQL_LOG') && IS_MYSQL_LOG==true){
                    writeLog($this->sql, date('Y-m-d').'_'.'sql.log');
                }
 		
	}


	/**
	 * 获取执行SQL的时间
	 *
	 * @access public
	 * @return void
	 */
	public function getTime()
	{
		echo '<div align=center>Process: '.number_format((array_sum(split(' ', microtime())) - $this->endTime), 6).'s</div>';
	}
}
?>